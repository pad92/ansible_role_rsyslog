Role Name
=========

This role configure Rsyslog.

Requirements
------------

modern distribution already have rsyslog installed, this role should not required anything else

Role Variables
--------------
```
rsyslog:
  tcp:
  clm: "servername"
  defaults:
  customconfig:
    - "# Managed by Ansible"
    - "# "
    - "# set stuff through ansible to expend central-log-manager capabilities"
  disabled: "yes"
```

If sending logs to another serveur, set the recever (log manager) in `clm` var. you'll also need to choose TCP or UDP as lvl4 protocol, in doubt, choose tcp.
if you set tcp or udp without a clm servername, the role will fail.

`disabled` will stop rsyslog & disable it, it'll still be installed though.

Dependencies
------------

none

License
-------

BSD
